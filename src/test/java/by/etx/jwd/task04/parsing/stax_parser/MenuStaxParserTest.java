package by.etx.jwd.task04.parsing.stax_parser;

import by.etc.jwd.task04.domain.Dish;
import by.etc.jwd.task04.domain.DishOption;
import by.etc.jwd.task04.domain.MenuDivision;
import by.etc.jwd.task04.domain.Photo;
import by.etc.jwd.task04.parsing.stax_parser.MenuStaxParser;
import by.etc.jwd.task04.parsing.stax_parser.MenuStaxContentHandler;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class MenuStaxParserTest {

    @Test
    public void parse_shouldReturnProperList() {
        DishOption dishOption = new DishOption();
        dishOption.setDescription("Слабосоленая форель, семга");
        dishOption.setPrice(150.18);
        dishOption.setPortion("100/20/15");

        List<DishOption> dishOptions = new ArrayList<>();
        dishOptions.add(dishOption);

        Dish dish = new Dish();
        dish.setId("d1001");
        dish.setTitle("Ассорти рыбное");
        Photo photo = new Photo();
        photo.setType("jpg");
        photo.setImageName("fishPlatter.jpg");
        photo.setImagePath("http://www.etc.by/menu/images");
        dish.setImage(photo);
        dish.setDishOptions(dishOptions);

        List<Dish> dishes = new ArrayList<>();
        dishes.add(dish);

        MenuDivision menuDivision = new MenuDivision();
        menuDivision.setDivisionTitle("Холодные закуски");
        menuDivision.setDishes(dishes);

        List<MenuDivision> expected = new ArrayList<>();
        expected.add(menuDivision);

        MenuStaxParser parser = new MenuStaxParser(new MenuStaxContentHandler());
        List<MenuDivision> actual = null;

        try {
            Path xmlFilePath = Paths.get(this.getClass().getClassLoader().getResource("menu.xml").toURI());
            actual = parser.parseDocument(xmlFilePath.toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(actual, expected);
    }
}
