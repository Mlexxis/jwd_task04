package by.etc.jwd.task04;

import by.etc.jwd.task04.domain.MenuDivision;
import by.etc.jwd.task04.parsing.dom_parser.MenuDomParser;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Launcher {
    public static void main(String[] args) {

        /*SaxParser parser = new SaxParser(new MenuSaxContentHandler());
        List<MenuDivision> list = new ArrayList<>();
        try {
            Path xmlFilePath = Paths.get(Launcher.class.getClassLoader().getResource("menu.xml").toURI());
            list = parser.parseDocument(xmlFilePath.toString());
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        //System.out.println(list);


        for (MenuDivision menuDivision : list) {
            System.out.println(menuDivision.getDivisionTitle());
            for (Dish dish : menuDivision.getDishes()) {
                System.out.println(dish.getId());
                System.out.println(dish.getTitle());
                System.out.println(dish.getImage().getImageName());
                System.out.println(dish.getImage().getImagePath());
                for (DishOption dishOption : dish.getDishOptions()) {
                    System.out.println(dishOption.getDescription());
                    System.out.println(dishOption.getPrice());
                    System.out.println(dishOption.getPortion());
                }
            }
        }*/

        /*MenuStaxParser parser = new MenuStaxParser(new MenuStaxContentHandler());
        List<MenuDivision> list = new ArrayList<>();
        try {
            Path path = Paths.get(Launcher.class.getClassLoader().getResource("menu.xml").toURI());

            list = parser.parseDocument(path.toString());


        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        for (MenuDivision menuDivision : list) {
            System.out.println(menuDivision.getDivisionTitle());
            for (Dish dish : menuDivision.getDishes()) {
                System.out.println(dish.getId());
                System.out.println(dish.getTitle());
                System.out.println(dish.getImage().getImageName());
                System.out.println(dish.getImage().getImagePath());
                for (DishOption dishOption : dish.getDishOptions()) {
                    System.out.println(dishOption.getDescription());
                    System.out.println(dishOption.getPrice());
                    System.out.println(dishOption.getPortion());
                }
            }
        }*/

        List<MenuDivision> list = new ArrayList<>();

        try {
            Path path = Paths.get(Launcher.class.getClassLoader().getResource("menu.xml").toURI());
            MenuDomParser parser = new MenuDomParser();
            parser.parseDocument(path.toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
