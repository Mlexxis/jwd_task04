package by.etc.jwd.task04.domain;

import java.io.Serializable;
import java.util.List;

public class Dish implements Serializable {

    private static final long serialVersionUID = -1215649709864171622L;
    private String id;
    private String title;
    private Photo image;
    private List<DishOption> dishOptions;

    public Dish() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Photo getImage() {
        return image;
    }

    public void setImage(Photo image) {
        this.image = image;
    }

    public List<DishOption> getDishOptions() {
        return dishOptions;
    }

    public void setDishOptions(List<DishOption> dishOptions) {
        this.dishOptions = dishOptions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dish dish = (Dish) o;

        if (title != null ? !title.equals(dish.title) : dish.title != null) return false;
        if (image != null ? !image.equals(dish.image) : dish.image != null) return false;
        return dishOptions != null ? dishOptions.equals(dish.dishOptions) : dish.dishOptions == null;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + (this.title == null ? 0 : title.hashCode())
                + (this.dishOptions == null ? 0 : dishOptions.hashCode())
                + (this.image == null ? 0 : image.hashCode());
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Dish{");
        sb.append("title='").append(title).append('\'');
        sb.append(", image=").append(image);
        sb.append(", dishOptions=").append(dishOptions);
        sb.append('}');
        return sb.toString();
    }
}
