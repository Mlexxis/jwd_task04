package by.etc.jwd.task04.domain;

import java.io.Serializable;

public class DishOption implements Serializable {

    private static final long serialVersionUID = 4471147007649397574L;
    private String description;
    private double price;
    private String portion;

    public DishOption() {};

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPortion() {
        return portion;
    }

    public void setPortion(String portion) {
        this.portion = portion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DishOption that = (DishOption) o;

        if (Double.compare(that.price, price) != 0) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return portion != null ? portion.equals(that.portion) : that.portion == null;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + (int) this.price + (this.description == null ? 0 : description.hashCode()) + (this.portion == null ? 0 : portion.hashCode());
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DishOption{");
        sb.append("description='").append(description).append('\'');
        sb.append(", price=").append(price);
        sb.append(", portion='").append(portion).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
