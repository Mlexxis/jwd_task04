package by.etc.jwd.task04.domain;

import java.io.Serializable;
import java.util.List;

public class MenuDivision implements Serializable {

    private static final long serialVersionUID = 8496563797881979964L;
    private String divisionTitle;
    private List<Dish> dishes;

    public MenuDivision() {
    }

    public String getDivisionTitle() {
        return divisionTitle;
    }

    public void setDivisionTitle(String divisionTitle) {
        this.divisionTitle = divisionTitle;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuDivision that = (MenuDivision) o;

        if (divisionTitle != null ? !divisionTitle.equals(that.divisionTitle) : that.divisionTitle != null)
            return false;
        return dishes != null ? dishes.equals(that.dishes) : that.dishes == null;
    }

    @Override
    public int hashCode() {
        int result = divisionTitle != null ? divisionTitle.hashCode() : 0;
        result = 31 * result + (dishes != null ? dishes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MenuDivision{");
        sb.append("divisionTitle='").append(divisionTitle).append('\'');
        sb.append(", dishes=").append(dishes);
        sb.append('}');
        return sb.toString();
    }
}
