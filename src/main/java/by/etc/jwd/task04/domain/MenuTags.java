package by.etc.jwd.task04.domain;

public enum MenuTags {
    MENU {
        public String getTagName () {
            return "menu";
        }
    }, MENUDIVISION {
        public String getTagName () {
            return "menuDivision";
        }
    }, DIVISIONTITLE {
        public String getTagName () {
            return "divisionTitle";
        }
    }, DISHES {
        public String getTagName () {
            return "dishes";
        }
    }, DISH {
        public String getTagName () {
            return "dish";
        }
    }, TITLE{
        public String getTagName () {
            return "title";
        }
    }, PHOTO {
        public String getTagName () {
            return "photo";
        }
    }, IMAGENAME {
        public String getTagName () {
            return "imageName";
        }
    }, IMAGEPATH {
        public String getTagName () {
            return "imagePath";
        }
    }, DISHOPTIONS {
        public String getTagName () {
            return "dishOptions";
        }
    }, DISHOPTION {
        public String getTagName () {
            return "idishOption";
        }
    }, DESCRIPTION {
        public String getTagName () {
            return "description";
        }
    }, PRICE {
        public String getTagName () {
            return "price";
        }
    }, PORTION {
        public String getTagName () {
            return "portion";
        }
    };

    public abstract String getTagName();
}
