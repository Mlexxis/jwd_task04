package by.etc.jwd.task04.domain;

import java.io.Serializable;
import java.net.URI;

public class Photo implements Serializable {

    private static final long serialVersionUID = 3249819488172055542L;
    private String imageName;
    private String imagePath;
    private String type;

    public Photo() {};

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Photo photo = (Photo) obj;

        if (imageName != null && !imageName.equals(photo.imageName) || imageName == null && photo.imageName != null) return false;
        return imagePath != null && imagePath.equals(photo.imagePath) || imagePath == null && photo.imagePath == null;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + (this.imageName == null ? 0 : imageName.hashCode()) + (this.imagePath == null ? 0 : imagePath.hashCode());
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Photo{");
        sb.append("imageName='").append(imageName).append('\'');
        sb.append(", imagePath=").append(imagePath);
        sb.append('}');
        return sb.toString();
    }
}
