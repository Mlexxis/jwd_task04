package by.etc.jwd.task04.parsing.sax_parser;

import by.etc.jwd.task04.domain.MenuDivision;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import java.io.IOException;
import java.util.List;

public class MenuSaxParser {

    private MenuSaxContentHandler handler;

    public MenuSaxParser(MenuSaxContentHandler menuSaxContentHandler) {
        this.handler = menuSaxContentHandler;
    }

    public List<MenuDivision> parseDocument(String uri) throws SAXException, IOException {
        XMLReader reader = XMLReaderFactory.createXMLReader();
        InputSource source = new InputSource(uri);
        reader.setContentHandler(handler);
        reader.parse(source);
        return handler.getMenuDivisions();
    }
}
