package by.etc.jwd.task04.parsing.sax_parser;

import by.etc.jwd.task04.domain.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MenuSaxContentHandler extends DefaultHandler {

    private List<MenuDivision> menuDivisions;
    private MenuDivision menuDivision;
    private List<Dish> dishes;
    private Dish dish;
    private Photo photo;
    private List<DishOption> dishOptions;
    private DishOption dishOption;
    private  StringBuilder builder;

    public List<MenuDivision> getMenuDivisions() {
        return menuDivisions;
    }

    @Override
    public void startDocument() throws SAXException {
        //TODO
    }

    @Override
    public void endDocument() throws SAXException {
        //TODO
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        builder = new StringBuilder();
        MenuTags openingTag = MenuTags.valueOf(qName.toUpperCase());
        switch (openingTag) {
            case MENU: {
                menuDivisions = new ArrayList<>();
                break;
            }
            case MENUDIVISION: {
                menuDivision = new MenuDivision();
                break;
            }
            case DISHES: {
                dishes = new ArrayList<>();
                break;
            }

            case DISH: {
                dish = new Dish();
                dish.setId(attributes.getValue("id"));
                break;
            }
            case PHOTO: {
                photo = new Photo();
                photo.setType(attributes.getValue("imageType"));
            }
            case DISHOPTIONS: {
                dishOptions = new ArrayList<>();
                break;
            }
            case DISHOPTION: {
                dishOption = new DishOption();
                break;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        MenuTags tagName = MenuTags.valueOf(qName.toUpperCase());
        switch (tagName) {
            case DIVISIONTITLE: {
                    menuDivision.setDivisionTitle(builder.toString());
            }
            case DISHES: {
                menuDivision.setDishes(dishes);
                break;
            }
            case MENUDIVISION: {
                menuDivisions.add(menuDivision);
                menuDivision = null;
                break;
            }
            case TITLE: {
                dish.setTitle(builder.toString());
                break;
            }
            case IMAGENAME: {
                photo.setImageName(builder.toString());
                break;
            }
            case IMAGEPATH: {
                photo.setImagePath(builder.toString());
            }
            case DISHOPTIONS: {
                dish.setDishOptions(dishOptions);
                dishOptions = null;
                break;
            }
            case DISH: {
                dishes.add(dish);
                dish = null;
                break;
            }
            case PHOTO: {
                dish.setImage(photo);
                photo = null;
                break;
            }
            case DESCRIPTION: {
                dishOption.setDescription(builder.toString());
                break;
            }
            case PRICE: {
                dishOption.setPrice(Double.parseDouble(builder.toString()));
                break;
            }
            case PORTION: {
                dishOption.setPortion(builder.toString());
                break;
            }
            case DISHOPTION: {
                dishOptions.add(dishOption);
                dishOption = null;
                break;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        builder.append(ch, start, length);
    }
}
