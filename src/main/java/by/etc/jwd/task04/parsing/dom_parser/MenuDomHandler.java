package by.etc.jwd.task04.parsing.dom_parser;

import by.etc.jwd.task04.domain.*;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class MenuDomHandler {

    private final int SINGLE_ELEMENT = 0;
    private List<MenuDivision> menuDivisions;
    private MenuDivision menuDivision;
    private List<Dish> dishes;
    private Dish dish;
    private Photo photo;
    private List<DishOption> dishOptions;
    private DishOption dishOption;

    public void createMenuDivisionsList() {
        menuDivisions = new ArrayList<>();
    }

    public void createMenuDivision(Element element) {
        this.menuDivision = new MenuDivision();
        String divisionTitle = element.getElementsByTagName(MenuTags.DIVISIONTITLE.getTagName()).item(SINGLE_ELEMENT).getTextContent();
        menuDivision.setDivisionTitle(divisionTitle);

    }

    public void createDish(Element element) {

    }

    public void createDishOption(NodeList dishOption) {

    }



}
