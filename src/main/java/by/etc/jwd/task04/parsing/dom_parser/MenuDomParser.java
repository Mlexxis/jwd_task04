package by.etc.jwd.task04.parsing.dom_parser;

import by.etc.jwd.task04.domain.*;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MenuDomParser {

    private final int SINGLE_ELEMENT = 0;

    public List<MenuDivision> parseDocument(String uri) throws IOException, SAXException {
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(uri));
        Document document = parser.getDocument();
        Element root = document.getDocumentElement();

        NodeList menuDivisionsNode = root.getElementsByTagName(MenuTags.MENUDIVISION.getTagName());
        List<MenuDivision> menuDivisions = new ArrayList<>();
        for (int i = 0; i < menuDivisionsNode.getLength(); i ++) {
            Element menuDivision = (Element) menuDivisionsNode.item(i);
            menuDivisions.add(createMenuDivision(menuDivision));
            NodeList dishesNode = menuDivision.getElementsByTagName(MenuTags.DISH.getTagName());
            List<Dish> dishes = new ArrayList<>();
            for (int j = 0; j < dishesNode.getLength(); j++) {
                Element dish = (Element) dishesNode.item(j);
                dishes.add(createDish(dish));
                NodeList dishOptionsNode = dish.getElementsByTagName(MenuTags.DISHOPTION.getTagName());
                List<DishOption> dishOptions = new ArrayList<>();
                for (int k = 0; k < dishOptionsNode.getLength(); k++) {
                    Element dishOptionElement = (Element) dishOptionsNode.item(k);
                    dishOptions.add(createDishOption(dishOptionElement));
                }
            }
        }
        return menuDivisions;
    }

    private MenuDivision createMenuDivision(Element element) {
        MenuDivision menuDivision = new MenuDivision();
        NodeList divisionTitle = element.getElementsByTagName(MenuTags.DIVISIONTITLE.getTagName());
        menuDivision.setDivisionTitle(divisionTitle.item(SINGLE_ELEMENT).getTextContent());
        return menuDivision;
    }

    private Dish createDish(Element element) {
        Dish dish = new Dish();
        dish.setId(element.getAttribute("id"));
        NodeList title = element.getElementsByTagName(MenuTags.TITLE.getTagName());
        dish.setTitle(title.item(SINGLE_ELEMENT).getTextContent());
        Photo photo = new Photo();
        NodeList photoNode = element.getElementsByTagName(MenuTags.PHOTO.getTagName());
        //photo.setType(photoNode.);
        Node imageNameNode = photoNode.item(0);
        Node imagePathNode = photoNode.item(1);
        photo.setImageName(imageNameNode.getTextContent());
        photo.setImagePath(imagePathNode.getTextContent());
        return dish;
    }

    private DishOption createDishOption(Element element) {
        DishOption dishOption = new DishOption();
        NodeList description = element.getElementsByTagName(MenuTags.DESCRIPTION.getTagName());
        dishOption.setDescription(description.item(SINGLE_ELEMENT).getTextContent());
        NodeList price = element.getElementsByTagName(MenuTags.PRICE.getTagName());
        dishOption.setPrice(Double.parseDouble(price.item(SINGLE_ELEMENT).getTextContent()));
        NodeList portion = element.getElementsByTagName(MenuTags.PORTION.getTagName());
        dishOption.setPortion(portion.item(SINGLE_ELEMENT).getTextContent());
        return dishOption;
    }
}
