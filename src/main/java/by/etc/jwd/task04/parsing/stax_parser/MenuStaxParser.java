package by.etc.jwd.task04.parsing.stax_parser;

import by.etc.jwd.task04.domain.MenuDivision;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class MenuStaxParser {

    private final int FIRST_ATTRIBUTE = 0;
    private MenuStaxContentHandler handler;

    public MenuStaxParser(MenuStaxContentHandler handler) {
        this.handler = handler;
    }

    public List<MenuDivision> parseDocument(String uri) throws XMLStreamException, FileNotFoundException {
        XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new FileInputStream(uri));
        while (reader.hasNext()) {
            int tagType = reader.next();
            switch (tagType) {
                case XMLStreamConstants.START_ELEMENT: {
                    String attributeValue = reader.getAttributeValue(FIRST_ATTRIBUTE);
                        if (attributeValue == null) {
                            handler.startElement(reader.getLocalName());
                        } else handler.startElement(reader.getLocalName(), attributeValue.trim());
                    break;
                }
                case XMLStreamConstants.CHARACTERS: {
                    String content = reader.getText().trim();
                    if (!content.isEmpty()) {
                        handler.characters(content);
                    }
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                        handler.endElement(reader.getLocalName());
                }
            }
        }
        return handler.getMenuDivisions();
    }
}
