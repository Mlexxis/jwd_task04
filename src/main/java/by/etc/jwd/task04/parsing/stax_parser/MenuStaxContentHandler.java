package by.etc.jwd.task04.parsing.stax_parser;

import by.etc.jwd.task04.domain.*;

import java.util.ArrayList;
import java.util.List;

public class MenuStaxContentHandler {

    private List<MenuDivision> menuDivisions;
    private MenuDivision menuDivision;
    private List<Dish> dishes;
    private Dish dish;
    private Photo photo;
    private List<DishOption> dishOptions;
    private DishOption dishOption;
    private MenuTags tag;

    public List<MenuDivision> getMenuDivisions() {
        return menuDivisions;
    }

    public void characters(String content) {
        switch (tag) {
            case DIVISIONTITLE: {
                menuDivision.setDivisionTitle(content);
                break;
            }
            case TITLE: {
                dish.setTitle(content);
                break;
            }
            case IMAGENAME: {
                photo.setImageName(content);
                break;
            }
            case IMAGEPATH: {
                photo.setImagePath(content);
                break;
            }
            case DESCRIPTION: {
                dishOption.setDescription(content);
                break;
            }
            case PRICE: {
                dishOption.setPrice(Double.parseDouble(content));
                break;
            }
            case PORTION: {
                dishOption.setPortion(content);
                break;
            }
        }
    }

    public void startElement(String tagName) {
        tag = MenuTags.valueOf(tagName.toUpperCase());
        switch (tag) {
            case MENUDIVISION: {
                menuDivision = new MenuDivision();
                break;
            }
            case DISHES: {
                dishes = new ArrayList<>();
                break;
            }
            case DISHOPTIONS: {
                dishOptions = new ArrayList<>();
                break;
            }
            case DISHOPTION: {
                dishOption = new DishOption();
                break;
            }

        }
    }

    public void endElement(String tagName) {
        tag = MenuTags.valueOf(tagName.toUpperCase());
        switch (tag) {
            case PHOTO: {
                dish.setImage(photo);
                photo = null;
                break;
            }
            case DISHOPTION: {
                dishOptions.add(dishOption);
                dishOption = null;
                break;
            }
            case DISHOPTIONS: {
                dish.setDishOptions(dishOptions);
                dishOptions = null;
                break;
            }
            case DISH: {
                dishes.add(dish);
                dish = null;
                break;
            }
            case DISHES: {
                menuDivision.setDishes(dishes);
                dishes = null;
                break;
            }
            case MENUDIVISION: {
                menuDivisions.add(menuDivision);
                menuDivision = null;
                break;
            }
        }
    }

    public void startElement(String tagName, String attributeValue) {
        tag = MenuTags.valueOf(tagName.toUpperCase());
        switch (tag) {
            case MENU: {
                menuDivisions = new ArrayList<>();
            }
            case DISH: {
                dish = new Dish();
                dish.setId(attributeValue);
                break;
            }
            case PHOTO: {
                photo = new Photo();
                photo.setType(attributeValue);
            }
        }
    }

}
